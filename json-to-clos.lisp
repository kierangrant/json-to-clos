(in-package :json-to-clos)

;;; Private

#|
Class Mapping Form
(class-symbol-or-form ({item-mapping}*) &key finally)

item-mapping
(from-form to-form {:mapping class-mapping-form})

If :finally is specified, it must be a function that is called with the completed object.
It's return value is stored instead of the return value. This allows special overrides and finalisation to be done.

If the :mapping keyword is present, it presents a NEW class-mapping to take
effect for the duration of decoding this field.

If to-form is a Function (and from-form is NOT T), then it must be a writer. (lambda (value instance))
This means it can be a writer, accessor or another function.

If from-form is T, then to-form MUST be a function of signature:
(instance key value)
Where instance is the instance of Object, for value to be added to, with key as the JSON Object key name for this field.

Takes a form that describes a class mapping, and build a form that will return a hash-table.

EG:
('person
 (("Name" 'name)
  ("Assets" 'assets
   :mapping
    ('asset
     (("Currency" 'currency)
      ("Value" 'value))))
  ("Dict" 'dict
   :mapping
   (#'(lambda () (make-hash-table :test #'equal))
    ((t (lambda (instance key value) (setf (gethash key instance) value))))
    :finally (lambda (value) (format t "HT: ~D~%" (hash-table-count value)) value)))))
->
(let ((#:mapping-processor-0 (list :map (make-hash-table :test #'equal))))
  (set (getf #:mapping-processor-0 :class) 'person)
  (setf (gethash "Name" (getf #:mapping-processor-0 :map)) (cons 'name nil))
  (setf (gethash "Assets" (getf #:mapping-processor-0 :map))
	(cons
	 'assets
	 (let ((#:mapping-processor-1 (list :map (make-hash-table :test #'equal))))
	   (setf (getf #:mapping-processor-1 :class) 'asset)
	   (setf (gethash "Currency" (getf #:mapping-processor-1 :map)) (cons 'currency nil))
	   (setf (gethash "Value" (getf #:mapping-processor-1 :map)) (cons 'value nil))
	   #:mapping-processor-1)))
  (setf (gethash "Dict" #:map)
	(cons
	 'dict
	 (let ((#:mapping-processor-2 (list :map (make-hash-table :test #'equal))))
	   (setf (getf #:mapping-processor-2 :class) #'(lambda () (make-hash-table :test #'equal)))
	   (setf (getf #:mapping-processor-2 :finally) #'(lambda (value) (format t "HT size:~D~%" (hash-table-count value)) value))
	   (setf (gethash t (getf #:mapping-processor-2 :map)) (cons #'(lambda (instance key value) (setf (gethash key instance) value))))
	   #:mapping-processor-2)))
  #:mapping-processor-0)

It should be a form that can create a hash-table that can be parsed.
:class is the special entry that names the class of the instance to be created,
for every field found in the JSON object, if it matches an entry in the hash-table, it will
be assigned to that slot, but if the value in the hash-table denotes a Function,
then the decoded JSON object will be passed to that function.
The function must accept two values: The decoded value and the instance to apply it to.

If the field was T though, it is a general function for custom handling, of signature
(lambda (instance key value)) instead of (lambda (value instance))

When looking up a field defintion, if it contains a new mapping (the CDR) it will
be applied for the duration of processing that field.

|#

;; Takes a mapping form, and returns a hash-table that is used by the processor functions
(eval-when (:compile-toplevel :load-toplevel)
  (defun %map-processor (mapping)
    (let ((map (gensym "MAPPING-PROCESSOR-")))
      `(let ((,map (list :map (make-hash-table :test #'equal))))
	 ,@(destructuring-bind (class-form mappings &key finally) mapping
	     `((setf (getf ,map :class) ,class-form)
	       ,@(if finally `((setf (getf ,map :finally) ,finally)))
	       ,@(loop for sub-map in mappings collect
		      (destructuring-bind (key field &key mapping) sub-map
			`(setf (gethash ,key (getf ,map :map))
			       (cons ,field ,(if mapping (%map-processor mapping))))))))
	 ,map))))

;; Simple macro around %map-processor so that you don't need to quote mapping
;; locally binds *gensym-counter* to 0 so you can easily see nesting in symbol names
(defmacro map-processor (mapping)
  (let ((*gensym-counter* 0))
    (%map-processor mapping)))

(defvar *current-mapping* nil "Current Mapping in effect")
(defvar *current-object* nil "List of Objects, CAR of which is the current object. If you cannot SETF to the passed value for a handler function, you can SETF to (CAR *CURRENT-OBJECT*) instead.")
(defvar *current-key* nil "List of keys. CAR of which is the current key. For a key of a MAPPING it is (CONS KEY NIL) where KEY is the REAL key.")
(defvar *current-tracker* nil "List of Object Keys. CAR of which is the Pseudo-key for the current object. When (EQ (CAR *CURRENT-TRACKER*) (CAR *CURRNET-KEY*)) this means that the *CURRENT-MAPPING* (along with *CURRENT-TRACKER* and *CURRENT-KEY* will be POPed after it is returned).")

(define-symbol-macro current-mapping (car *current-mapping*))
(define-symbol-macro current-key (car *current-key*))
(define-symbol-macro current-tracker (car *current-tracker*))
(define-symbol-macro current-object (car *current-object*)) ; PUBLIC
(define-symbol-macro current-maps (getf current-mapping :map))
(define-symbol-macro current-class (getf current-mapping :class))
(define-symbol-macro current-finally (getf current-mapping :finally)) ; PUBLIC
(defun %map-key->form (key) (car (gethash key current-maps)))
(defun %map-key->map (key) (cdr (gethash key current-maps)))

;; At beginning of object, either create an instance if it is a symbol, otherwise call a function to create
;; ths initial object. Returns (unused) new object (useful for trace)
(defun %mapper-beginning-of-object ()
  (push (if (functionp current-class) (funcall current-class) (make-instance current-class)) *current-object*)
  current-object)

;; When key is in mapping, we are either keeping current mapping, or we are loading a new mapping
;; If the key is NOT in mapping, then we need to check to see if there is a catch-all handler (maybe mapping)
(defun %mapper-object-key-handler (key)
  (cond
    ((%map-key->map key)		; has new mapping
     (push (cons key nil) *current-key*)
     (push current-key *current-tracker*)
     (push (%map-key->map key) *current-mapping*))
    ((%map-key->map t)			; default has new mapping
     (push (cons key nil) *current-key*)
     (push current-key *current-tracker*)
     (push (%map-key->map t) *current-mapping*))
    (t (push key *current-key*)))	; if no mapping or non-existent key
  current-key)

;; Called to attach a value (whether a mapping or not) to (car *current-object*)
;; Try (gethash key (car *current-mapping*)), then check (gethash T (car *current-mapping*))
;; Doesn't pop values from lexical stack, that is callers job
;; It is %mapper-object-value-handler's job to extract the key name for a finishing enclosing mapping
(defun %attach-value-to-object (key value)
  (cond
    ((gethash key current-maps)
     (cond ((functionp (%map-key->form key)) (funcall (%map-key->form key) value current-object))
	   (t (setf (slot-value current-object (%map-key->form key)) value))))
    ((gethash t current-maps)
     (cond ((functionp (%map-key->form t)) (funcall (%map-key->form t) current-object key value))
	   (t (error "Catch-all handler must be a function if specified")))))
  nil)

;; When this is called, it is time to add the value to the object.
;; If the current *current-key* is the top value of *current-tracker* this means that
;; we are finished with this current mapping. Otherwise this binding is still in effect.
;; This is if we are collecting values inside an object that nests itself OR we are collecting
;; objects into a Vector, that don't have mappings.
;; That is, you can have a binding on a field for a new type, but it actually stores a vector of those objects
;; this results in a new binding for that field, and just new objects for each sub-object, same mapping
(defun %mapper-object-value-handler (value)
  ;; If (eq current-key current-tracker) we have hit the return of this mapping.
  ;; There is no entry for root item in the stack, so this should never be called on finishing the
  ;; top-level object. For all other items, if the key exists in mapping, we assign to it
  (cond
    ((eq current-key current-tracker)
     ;; End of a mapping, time to save it to current-object. (car current-key) is the actual field to save to
     ;; First need to pop off old mapping, as we are really searching now under parent mapping for curernt object
     (pop *current-mapping*)
     (%attach-value-to-object (car current-key) value)
     ;; After we have processed the sub-mapper. Restore rest of state. I think this could actually be done before...
     (pop *current-tracker*)	; pop off the tracker
     (pop *current-key*))	; pop off from the key list
    (t
     (%attach-value-to-object (pop *current-key*) value)))
  nil)

(defun %mapper-end-of-object-handler ()
  (typecase current-finally
    (cons (if (car current-finally) (funcall (car current-finally) (pop *current-object*))))
    (function (funcall current-finally (pop *current-object*)))
    (t (pop *current-object*))))

;;; Public

(defmacro with-json-to-clos ((mapping) &body body)
  `(json:with-decoder-simple-list-semantics
     (let* ((*current-mapping* (list ,(let ((*gensym-counter* 0)) (%map-processor mapping))))
	    (*current-object*) (*current-key*) (*current-tracker*)
	    (json:*beginning-of-object-handler* #'%mapper-beginning-of-object)
	    (json:*object-key-handler* #'%mapper-object-key-handler)
	    (json:*object-value-handler* #'%mapper-object-value-handler)
	    (json:*end-of-object-handler* #'%mapper-end-of-object-handler))
       ,@body)))

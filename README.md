A Simple JSON to CLOS helper macro.

It is currently in early stages.

It allows you to specify a custom mapping from JSON objects to CLOS objects or other custom objects by instead specifying a custom function to create the initial object.

You specify which JSON fields match which CLOS slots in a class type, or even specify a function instead to be called to assign that value to that object.

Such as #'(setf accessor) instead of slot-name.

You can also specify finalises to be called when an object is complete to transform it if necessary.

You specify that new mapping applies on certain fields, including nesting such mappings.

By default, a mapping has indefinite scope to it's descendants.

Example

```common-lisp
(defclass asset () ((name :accessor asset-name) (value :accessor asset-value)))
(defclass currency () ((code :accessor currency-code) (value :accessor currency-value)))
(defmethod print-object ((object asset) stream)
  (format stream "Asset Name: ~A, Value: ~A" (asset-name object) (asset-value object)))
(defmethod print-object ((object currency) stream)
  (format stream "~A$~A" (currency-code object) (currency-value object)))

(json-to-clos:with-json-to-clos
    (('asset
      (("name" #'(setf asset-name))
       ("value" #'(setf asset-value)
		:mapping
		('currency
		 (("code" #'(setf currency-code))
		  ("value" #'(setf currency-value))))))))
  (json:decode-json-from-string
   "{\"name\": \"House\", \"value\": {\"code\": \"AUD\", \"value\": 1000000.0}}"))
->
Asset Name: House, Value: AUD$1000000.0
```

If you specify functions for the class-form, or the setter functions, the Symbol macros CURRENT-OBJECT and CURRENT-FINALLY are defined to allow you to directly access the current object (for cases where SETF on the passed object do not work as expected) and to override the Finally form based on a key value inside the object.

Example

```common-lisp
(defclass my-currency () ((code :accessor code :initarg :code :initform nil)
			  (value :accessor value :initarg :value :initform nil)))
(defclass my-date () ((value :accessor value :initarg :value :initform nil)))
(defclass my-time () ((value :accessor value :initarg :value :initform nil)))

(json-to-clos:with-json-to-clos
    ((#'(lambda () (make-hash-table :test #'equal))
	((t #'(lambda (instance key value)
		(declare (ignore instance))
		;; In this example we actually can use instance because it is a HASH-TABLE
		(if (and (string= key "_type")
			 (member value '("Currency" "Date" "Time") :test #'equal))
		    (setf json-to-clos:current-finally
			  #'(lambda (object)
			      (setf json-to-clos:current-finally nil)
			      (cond
				((string= (gethash "_type" object) "Currency")
				 (make-instance 'my-currency
						:code (gethash "code" object)
						:value (gethash "value" object)))
				((string= (gethash "_type" object) "Date")
				 (make-instance 'my-date :value (gethash "value" object)))
				((string= (gethash "_type" object) "Time")
				 (make-instance 'my-time :value (gethash "value" object)))
				(t (error "Error Parsing Object"))))))
		(setf (gethash key json-to-clos:current-object) value))))))
  (json:decode-json-from-string
   "
[{\"foo\": 100},
 {\"_type\": \"Currency\", \"code\": \"AUD\", \"value\": \"1000.23\"},
 {\"_type\": \"Date\", \"value\": \"2018-06-06\"}]"))
 ->
(#<HASH-TABLE :TEST EQUAL :COUNT 1 {100B01A5D3}> #<MY-CURRENCY {100B0A84D3}>
 #<MY-DATE {100B128D43}>)
```

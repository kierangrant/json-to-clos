(defsystem "json-to-clos"
  :description "A simply macro to convert JSON objects to CLOS objects"
  :depends-on ("cl-json")
  :author "Kieran Grant"
  :version "0.0.3"
  :license "Public Domain"
  :serial t
  :components
  ((:file "package")
   (:file "json-to-clos")))

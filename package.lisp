(defpackage :json-to-clos
  (:use :cl)
  (:export #:with-json-to-clos #:current-finally #:current-object))
